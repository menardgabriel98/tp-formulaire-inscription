charsSpe = ['@', '^', '%', '$', '&', '+', '-', '/']
numbers = [1,2,3,4,5,6,7,8,9,0]

$('#confirm').click(function (){
    errorContent=""
    specialCharInPswd = false
    for(special of charsSpe){
        if($('#password').val().indexOf(special) !== -1){
            specialCharInPswd = true
        }
    }

    numberInPswd = false
    for(number of numbers){
        if($('#password').val().indexOf(number) !== -1){
            numberInPswd = true
        }
    }


    if($('#username').val().length<8){
        if($('#username').val()===""){
            errorContent+="Identifiant obligatoire<br>"
        }
        else {
            errorContent += "Identifiant trop court<br>"
        }
    }

    if($('#password').val().length<8){
        errorContent+="Mot de passe trop court trop court<br>"
    }

    if($('#mail').val().indexOf('@') === -1){
        errorContent+="Le mail est invalide<br>"
    }

    else if($('#mail').val().indexOf('@') > $('#mail').val().lastIndexOf('.') ){
        errorContent+="Le mail est invalide<br>"
    }

    if(!specialCharInPswd){
        errorContent+="Le mot de passe doit contenir au moin un caractère spécial<br>"
    }

    if(!numberInPswd){
        errorContent+="Le mot de passe doit contenir au moins un chiffre<br>"
    }

    if($('#confirm-password').val()!== $('#password').val()){
        errorContent+="Les mots de passe ne correspondent pas<br>"
    }

    if($('#tel').val().length!==10 || !$.isNumeric($('#tel').val())){
        errorContent+="Le téléphone est invalide<br>"
    }

    if(errorContent===""){
        $('.error').remove()
        $('.success').css("display", "block")
    }
    else {
        $('.error').html(errorContent+"<br>").css("display", "block");
    }


})